package com.tienda.entidades;

import com.tienda.entidades.Empelado;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-07-15T10:58:58")
@StaticMetamodel(Cargo.class)
public class Cargo_ { 

    public static volatile SingularAttribute<Cargo, Integer> codigoCargo;
    public static volatile ListAttribute<Cargo, Empelado> empeladoList;
    public static volatile SingularAttribute<Cargo, String> nombre;

}