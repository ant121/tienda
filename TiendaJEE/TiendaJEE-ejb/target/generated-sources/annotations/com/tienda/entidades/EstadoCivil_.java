package com.tienda.entidades;

import com.tienda.entidades.Empelado;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-07-15T10:58:58")
@StaticMetamodel(EstadoCivil.class)
public class EstadoCivil_ { 

    public static volatile SingularAttribute<EstadoCivil, Integer> codigoEstadoCivil;
    public static volatile ListAttribute<EstadoCivil, Empelado> empeladoList;
    public static volatile SingularAttribute<EstadoCivil, String> nombre;

}