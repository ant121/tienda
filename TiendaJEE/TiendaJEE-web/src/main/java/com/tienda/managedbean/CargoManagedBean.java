package com.tienda.managedbean;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import com.entidades.session.CargoFacadeLocal;
import com.tienda.entidades.Cargo;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;

/**
 *
 * @author antho
 */
@Named(value = "cargoManagedBean")
@ViewScoped
public class CargoManagedBean implements Serializable{
    
    //Paso 1
    @EJB
    private CargoFacadeLocal cargoFacadeLocal;
    
    //Una Lista para traer el listado de cargos
    private List<Cargo> listaCargos;    
    
    //Objeto de tipo Cargo
    private Cargo cargo;
    
    //Constructor
    public CargoManagedBean() {
    }
    
    
    //Paso 2
    //@PostConstruct
    public void init(){
        //lista de los cargos que estan en la BDD
        listaCargos = cargoFacadeLocal.findAll();
    }
    
    //@Override
    public void nuevo(){
        cargo = new Cargo();
    }
    
   // @Override
    public void grabar(){
        
        try{
            if(cargo.getCodigoCargo()== null){
            cargoFacadeLocal.create(cargo);
            }else{
                cargoFacadeLocal.edit(cargo);
            }
            cargo = null;
            listaCargos = cargoFacadeLocal.findAll();
            mostrarMensajeTry("Informacion OK", FacesMessage.SEVERITY_INFO);
        }catch(Exception e){
            mostrarMensajeTry("Ocurrio un error", FacesMessage.SEVERITY_ERROR);
        }   
    }

    private void mostrarMensajeTry(String ocurrio_un_error, FacesMessage.Severity SEVERITY_INFO) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    //@Override
    public void seleccionar(Cargo c){
        cargo = c;
    }
    
    //@Override
    public void eliminar(Cargo c){
        try{
            cargoFacadeLocal.remove(c);
            listaCargos = cargoFacadeLocal.findAll();
            mostrarMensajeTry("Informacion OK", FacesMessage.SEVERITY_INFO);
        }catch(Exception e){
            mostrarMensajeTry("Ocurrio un error", FacesMessage.SEVERITY_ERROR);
        }
        
    }
    
    //@Override
    public void cancelar(Cargo c){
        cargo = null;
    }

    public List<Cargo> getListaCargos() {
        return listaCargos;
    }

    public void setListaCargos(List<Cargo> listaCargos) {
        this.listaCargos = listaCargos;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }
    
    
}
